<?php


namespace Codr\Cass\Assemblers;

use Codr\Cass\Cass;
use Codr\Cass\Adapters\CassAdapter;

class Model
{
    /**
     * @var \Codr\Cass\Cass $model
     */
    private $model;

    /**
     * @var string
     */
    private $connection;

    /**
     * @var string
     */
    private $adapter;

    public function __construct()
    {
        $this->adapter = new CassAdapter();
    }

    public function setModel(Cass $model)
    {
        $this->model = $model;
        return $this;
    }

    /**
     * Find a record by its primary key
     *
     * @param string $id
     * @param array $fields
     * @return \Codr\Cass\Cass
     */
    public function find($id, $fields = ['*'])
    {
        $model = $this->getModel();

        $result = $this->adapter->select(
            $model->getScheme(),
            $model->getTable(),
            $fields,
            [[$model->getPrimaryKey(), "=", $id]]
        );

        if (!$result->count())
            return false;

        $model->{$model->getPrimaryKey()} = $id;
        foreach ($result as $row) {
            $model
                ->fill($row)
                ->setExists(true);
        }

        return $model;
    }

    public function insert()
    {
        $model = $this->getModel();

        $this->adapter->insert(
            $model->getScheme(),
            $model->getTable(),
            $model->getProperties()
        );

        return true;
    }

    public function update()
    {
        $model = $this->getModel();

        $this->adapter->update(
            $model->getScheme(),
            $model->getTable(),
            $this->getChanged(),
            [
                [$model->getPrimaryKey(), "=", $model->{$model->getPrimaryKey()}]
            ],
            $this->usePreparedStatements()
        );

        $model->fill($this->renderChanges());
        $model->changes = [];

        return true;

    }

    /**
     * @param string $cql
     * @return mixed
     */
    public function query($cql)
    {
        return $this->adapter->query($cql);
    }

    private function getChanged()
    {
        $model = $this->getModel();

        if (!$model->isCounter())
            return $model->getChanged();

        $changed = [];

        foreach ($model->getChanged() as $field => $value) {
            $operator = ($value > 0 ? "+" : "-");
            $count = abs($value);

            $changed[$field] = "$field $operator $count";
        }

        return $changed;
    }

    private function renderChanges()
    {
        $model = $this->getModel();

        // If not a counter, return plain changes.
        if (!$model->isCounter())
            return $model->getChanged();

        // If counter, add/sub the properties to the changes.
        $changed = [];


        foreach ($model->getChanged() as $field => $value) {
            $num = new \Cassandra\Bigint($model->{$field}->toInt() + $value);
            $changed[$field] = $num;
        }

        return $changed;
    }

    /**
     * Determine if prepared statements should be enabled
     *
     * @return bool
     */
    private function usePreparedStatements()
    {
        $model = $this->getModel();

        if ($model->isCounter())
            return false;

        return true;
    }

    private function &getModel()
    {
        return $this->model;
    }
}