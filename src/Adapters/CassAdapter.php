<?php

namespace Codr\Cass\Adapters;

use Codr\Cass\Interfaces\DatasourceAdapter;
use Cassandra;
use Ramsey\Uuid\Uuid;
use RuntimeException;

class CassAdapter implements DatasourceAdapter
{
    protected static $nodes;
    protected static $sessionFunction;
    protected static $session;
    protected static $sslCert;
    protected static $port = 9042;

    private $cluster    = [];
    private $connection;

    private $operators  = ["=", ">", "<", "<=", ">=", "IN"];

    public function __construct($connection = "default")
    {
        if (!isset(self::$nodes) && !isset(self::$sessionFunction))
            throw new \Exception("No nodes or sessionFunction defined");

        $this->connection = $connection;

        if (isset(self::$nodes)) {
            $builder = Cassandra::cluster()
                ->withContactPoints(self::$nodes)
                ->withPort(self::$port);
            
            if (isset(self::$sslCert)) {
                $ssl = Cassandra::ssl()
                    ->withVerifyFlags(Cassandra::VERIFY_PEER_CERT)
                    ->withTrustedCerts(self::$sslCert)
                    ->build();

                $builder->withSSL($ssl);
            }

            $this->cluster[$connection] = $builder->build();
        }
    }

    /**
     * Set a custom function that returns a \Cassandra\Session instance
     */
    public static function setSessionFunction($fn)
    {
        self::$sessionFunction = $fn;
    }

    public static function setNodes($nodes)
    {
        self::$nodes = $nodes;
    }

    public static function setSslCert($path)
    {
        if (!file_exists($path)) {
            throw new RuntimeException("Cannot locate certificate at $path");
        }

        self::$sslCert = $path;
    }

    public static function setPort($port) {
        self::$port = $port;
    }

    public function select($scheme, $table, $fields, $wheres = null, $limit = null)
    {
        $cql = "SELECT " . implode(",", $fields) . " FROM $table ";

        if (isset($wheres))
            $cql .= $this->whereClause($wheres);

        if (isset($limit))
            $cql .= "LIMIT $limit ";

        $session = $this->getSession($scheme);
        $statement      = $session->prepare($cql);

        return $this->execute($statement, $this->getBindsFromWheres($wheres));
    }

    public function insert($scheme, $table, $properties)
    {
        $values = substr(str_repeat("?,", count($properties)), 0, -1);
        $fields = implode(",", array_keys($properties));

        $cql = "INSERT INTO $table ($fields) VALUES ($values)";

        $session = $this->getSession($scheme);
        $statement      = $session->prepare($cql);

        return $this->execute($statement, $properties);
    }

    public function update($scheme, $table, $properties, $wheres = null, $preparedStatements = true)
    {
        $cql = "UPDATE $table SET {$this->setClause($properties,$preparedStatements)} ";

        if (isset($wheres))
            $cql .= $this->whereClause($wheres);

        $session = $this->getSession($scheme);

        $binds      = array_merge(($preparedStatements ? $properties : []), $this->getBindsFromWheres($wheres));
        $statement  = $session->prepare($cql);

        return $this->execute($statement, $binds);
    }

    public function delete()
    {
    }

    public function setDatasource($name, $datasource)
    {
        if (!isset($this->cluster[$name]))
            $this->cluster[$name] = $datasource;
    }

    private function simple($cql)
    {
        return false;
    }

    private function execute($statement, $binds)
    {
        $options = new Cassandra\ExecutionOptions(["arguments" => $binds]);
        $session = $this->getSession();

        return $session->execute($statement, $options);
    }

    private function getBindsFromWheres($wheres)
    {
        $binds = [];

        for ($i = 0; $i < count($wheres); $i++) {
            list($field, $operator, $value) = $wheres[$i];
            $binds[$field] = $value;
        }

        return $binds;
    }

    private function setClause($properties, $prepared = true)
    {
        $cql = "";

        foreach ($properties as $field => $value) {
            if ($prepared) {
                $cql .= "$field = ?,";
            } else {
                $cql .= "$field = $value,";
            }
        }

        return substr($cql, 0, -1);
    }

    private function whereClause($wheres)
    {
        for ($i = 0; $i < count($wheres); $i++) {
            list($field, $operator) = $wheres[$i];

            if ($i == 0) {
                $cql = "WHERE $field $operator ? ";
            } else {
                $cql = "AND $field $operator ? ";
            }
        }
        return $cql;
    }

    /**
     * Get the active cassandra cluster
     * @return \Cassandra
     */
    private function getCluster()
    {
        return $this->cluster[$this->connection];
    }

    private function getSession($keyspace = null)
    {
        if (!isset(self::$session)) {
            if (!isset($keyspace)) {
                throw new \Exception("No keyspace set!");
            }

            if (isset(self::$sessionFunction) && is_callable(self::$sessionFunction)) {
                self::$session = call_user_func(self::$sessionFunction, $keyspace);
            } else {
                $cluster = $this->cluster[$this->connection];
                self::$session = $cluster->connect($keyspace);
            }
        }

        if (is_bool(self::$session)) {
            throw new \Exception("Could not connect to cluster");
        }

        return self::$session;
    }
}
