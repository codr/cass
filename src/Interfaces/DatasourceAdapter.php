<?php

namespace Codr\Cass\Interfaces;

interface DatasourceAdapter {
    public function select($scheme, $table, $fields, $wheres, $limit);
    public function insert($scheme, $table, $data);
    public function update($scheme, $table, $properties, $wheres);
    public function delete();
    public function setDatasource($name, $datasource);
}