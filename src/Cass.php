<?php

namespace Codr\Cass;

use Cassandra\Exception\InvalidArgumentException;

abstract class Cass
{
    /**
     * @var string
     * Database table, defaults to lowercase class name
     */
    protected $table;

    /**
     * @var string
     * Primary key of table
     */
    protected $primaryKey = "id";

    /**
     * @var string
     * Which cluster connection to use, defaults to first defined.
     */
    protected $connection = "default";

    /**
     * @var string
     * Set scheme/keyspace
     */
    protected $scheme;


    /**
     * @var bool
     * Determines if the table is a counter
     */
    protected $counter = false;

    /**
     * @var integer
     * Amount to add/sub from a counter
     */
    protected $count_step = 1;

    /**
     * @var bool
     * Exists in datasource
     */
    private $exists = false;

    /**
     * @var array
     * Properties assigned through overloading
     */
    private $properties = [];

    /**
     * @var array
     * Changed properties through overloading
     */
    private $changed = [];

    public function __construct(array $properties = [])
    {
        $this->table = $this->table ?: strtolower(get_class($this));

        $this->setExists(false);
        $this->fill($properties);
    }

    public function save()
    {
        if ($this->exists) {
            return $this->update();
        } else {
            return $this->insert();
        }

        $this->changed = [];

        return true;
    }

    public function fill(array $properties = [])
    {
        foreach ($properties as $key => $value)
            $this->setProperty($key, $value);

        return $this;
    }

    public function setExists($bool)
    {
        $this->exists = $bool;
    }

    public function getScheme()
    {
        return $this->scheme;
    }

    public function getTable()
    {
        return $this->table;
    }

    public function getPrimaryKey()
    {
        return $this->primaryKey;
    }

    public function getProperties()
    {
        return $this->properties;
    }

    public function getChanged()
    {
        return $this->changed;
    }

    public function isCounter()
    {
        return $this->counter;
    }

    public function next($property)
    {
        return $this->bump($property, 1);
    }

    public function add($property, $amount = null)
    {
        return $this->bump($property, $amount);
    }

    public function subtract($property, $amount = null)
    {
        if (!isset($amount))
            $amount = $this->count_step;

        return $this->bump($property, (abs($amount) * -1));
    }

    public function bump($property, $amount = null)
    {
        if (!$this->isCounter())
            throw new \Exception("Method are only allowed on counter tables.");

        if (!isset($amount))
            $amount = $this->count_step;

        $this->changed[$property] = $amount;

        return $this->save();
    }

    public function __call($method, $arguments)
    {
        $model = new Assemblers\Model();
        $model->setModel($this);

        return call_user_func_array([$model, $method], $arguments);
    }

    public static function __callStatic($method, $arguments)
    {
        $instance = new static();

        return call_user_func_array([$instance, $method], $arguments);
    }

    /**
     * @param string $key
     * @return mixed
     */
    protected function getProperty($key)
    {
        if (array_key_exists($key, $this->properties))
            return $this->properties[$key];

        return false;
    }

    /**
     * Assign a property to the properties array, and to the changed
     * array if the record exists in the database, unless the table is a counter
     *
     * @param string $key
     * @param mixed $value
     */
    protected function setProperty($key, $value)
    {
        if ($this->exists && !$this->isCounter())
            $this->changed[$key] = $value;

        $this->properties[$key] = $value;
    }

    /**
     * Unset a property from the properties and changed array.
     *
     * @param string $key
     */
    public function unsetProperty($key)
    {
        unset($this->properties[$key]);
        unset($this->changed[$key]);
    }

    /**
     * Magic setter
     * Assign a property to the property array,
     * if the table is not a counter
     *
     * @param string $key
     * @param string $value
     */
    public function __set($key, $value)
    {
        if (!$this->isCounter())
            $this->setProperty($key, $value);
    }

    /**
     * Magic unsetter
     * Unset a property in the properties array, if the
     * table is not a counter
     *
     * @param string $key
     */
    public function __unset($key)
    {
        if (!$this->isCounter())
            $this->unsetProperty($key);
    }

    /**
     * Magic getter
     * @param string $key
     * @return mixed
     */
    public function __get($key)
    {
        return $this->getProperty($key);
    }
}
