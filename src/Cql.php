<?php

namespace Codr\Cass;

require '../vendor/autoload.php';

use Cassandra;
use Ramsey\Uuid;

class CQL
{
    private $cluster;
    private $session;

    protected $binds = [];
    protected $statement;

    public function __construct($hosts = [], $keyspace)
    {
        $this->cluster = Cassandra::cluster()
            ->withContactPoints(implode(",", $hosts))
            ->build();

        $this->session = $this->cluster->connect($keyspace);
    }

    public function prepare($statement)
    {
        $this->statement = $this->session->prepare($statement);

        return $this;
    }

    public function bind($binds)
    {
        array_push($this->binds, $binds);

        return $this;
    }

    public function insert()
    {
        if (empty($this->binds))
        {
            return false;
        }

        foreach ($this->binds as $bind)
        {
            $options = new Cassandra\ExecutionOptions(['arguments' => $bind]);
            $this->session->execute($this->statement, $options);
        }

        $this->clean();
        return true;
    }

    private function clean()
    {
        unset($this->statement);
        $this->binds = [];
    }

    private function getOption($type, $data)
    {
        return new Cassandra\ExecutionOptions([$type => $data]);
    }
}